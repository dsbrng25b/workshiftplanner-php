# workshiftplanner
## setup
### php backend
* `composer install`
* `php bin/console doctrine:database:create`
* `php bin/console doctrine:schema:create`

### javascript frontend
* `cd gui`
* `npm install`
* `npm run build`

## development
* `php bin/console server:run`
* `cd gui`
* `npm run dev`

## fetch opening hours

The following parameters need to be configured in `parameters.yml`:
* `ub_pub_uri`
* `ub_pub_location_id`
* `ub_pub_num_days_to_fetch`


```
php bin/console app:openinghours:update
```

This will fetch the opening hours and update the `Event` entries from the current system date until (and including) the calculated end date.
The end date is calculated with the parameter `ub_pub_num_days_to_fetch`.

It is possible to override this default values by passing the option `--date` and/or `--days` when calling this command.
