import axios from 'axios'

var http = axios.create({
  baseURL: '/api/user',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
})
export default {

  getUser () {
    return http.get().then((response) => {
      return response.data
    })
  }

  // login (username, password) {
  //   return http.post('/login').then((response) => {
  //     return response
  //   })
  // },
  //  // logout () {
  //   return http.post('/logout').then((response) => {
  //     return response
  //   })
  // }

}
