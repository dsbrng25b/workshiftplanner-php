import moment from 'moment'
import axios from 'axios'

export default class BaseService {
  constructor (apiEndpoint, config = {}) {
    this.http = axios.create({
      baseURL: apiEndpoint,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    this.config = Object.assign({
      timeFields: []
    }, config)
  }

  _parseDates (obj) {
    for (let field of this.config.timeFields) {
      if (obj[field] !== undefined) {
        let time = moment(obj[field])
        if (time.isValid()) {
          obj[field] = time
        } else {
          obj[field] = undefined
        }
      }
    }
    return obj
  }

  fromRest (obj) {
    return this._parseDates(obj)
  }

  toRest (obj) {
    return obj
  }

  getAll () {
    return this.http.get().then((response) => {
      return response.data.map(obj => this.fromRest(obj))
    })
  }

  get (id) {
    return this.http.get(id.toString()).then((response) => {
      return this.fromRest(response.data)
    })
  }

  _create (obj) {
    return this.http.post('', this.toRest(obj))
      .then((response) => {
        return this.fromRest(response.data)
      })
  }

  _update (obj) {
    return this.http.put(obj.id.toString(), this.toRest(obj))
      .then((response) => {
        return obj
      })
  }

  save (obj) {
    if (obj.id) {
      return this._update(obj)
    } else {
      return this._create(obj)
    }
  }

  remove (obj) {
    return this.http.delete(obj.id.toString())
  }
}
