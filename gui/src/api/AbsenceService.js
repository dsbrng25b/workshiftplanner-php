import CalendarEntryService from './CalendarEntryService'

export default class AbsenceService extends CalendarEntryService {
  constructor (config = {}) {
    super('/api/absence', config)
  }

  toRest (a) {
    return {
      start_time: a.start_time,
      end_time: a.end_time,
      reason: a.reason,
      optional: a.optional,
      person: a.person_id
    }
  }
}
