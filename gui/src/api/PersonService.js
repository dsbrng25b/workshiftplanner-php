import BaseService from './BaseService'

export default class PersonService extends BaseService {
  constructor (config = {}) {
    config = Object.assign({
      timeFields: [
        'contract_start',
        'contract_end'
      ]
    }, config)
    super('/api/person', config)
  }

  toRest (p) {
    return {
      name: p.name,
      username: p.username,
      is_admin: p.is_admin,
      short_name: p.short_name,
      color: p.color,
      employment_level: p.employment_level,
      contract_start: p.contract_start ? p.contract_start.format('YYYY-MM-DD') : null,
      contract_end: p.contract_end ? p.contract_end.format('YYYY-MM-DD') : null
    }
  }
}
