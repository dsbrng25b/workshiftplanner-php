import BaseService from './BaseService'

export default class CalendarEntryService extends BaseService {
  constructor (apiEndpoint, config) {
    config = Object.assign({
      timeFields: [
        'start_time',
        'end_time'
      ]
    }, config)
    super(apiEndpoint, config)
  }

  getByRange (start, end, calendar = true) {
    let config = {
      params: {
        start: start.format(),
        end: end.format(),
        calendar: calendar === true ? 'yes' : 'no'
      }
    }
    return this.http.get('', config).then((response) => {
      return response.data.map(obj => this.fromRest(obj))
    })
  }
}
