import Vue from 'vue'
import Router from 'vue-router'
import UbViewPerson from '@/components/UbViewPerson'
import UbViewCalendar from '@/components/UbViewCalendar'
import UbViewWork from '@/components/UbViewWork'
import UbViewStats from '@/components/UbViewStats'
import UbFormPerson from '@/components/UbFormPerson'
import UbFormWork from '@/components/UbFormWork'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Calendar',
      component: UbViewCalendar
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: UbViewCalendar
    },
    {
      path: '/stats',
      name: 'Statistik',
      component: UbViewStats
    },
    {
      path: '/person',
      name: 'PersonView',
      component: UbViewPerson
    },
    {
      path: '/person/new',
      name: 'PersonNew',
      component: UbFormPerson
    },
    {
      path: '/person/edit/:id',
      name: 'PersonEdit',
      component: UbFormPerson,
      props: true
    },
    {
      path: '/work',
      name: 'WorkView',
      component: UbViewWork
    },
    {
      path: '/work/new',
      name: 'WorkNew',
      component: UbFormWork
    },
    {
      path: '/work/edit/:id',
      name: 'WorkEdit',
      component: UbFormWork,
      props: true
    }
  ],
  linkExactActiveClass: 'is-active'
})
