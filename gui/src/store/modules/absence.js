import * as base from './util/base'
import * as range from './util/range'
import AbsenceService from '../../api/AbsenceService'

var api = new AbsenceService()

const state = {
  ...base.state(),
  ...range.state()
}

const getters = {
  ...base.getters,
  ...range.getters
}

const actions = {
  ...base.actions(api),
  ...range.actions(api)
}

const mutations = {
  ...base.mutations,
  ...range.mutations
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
