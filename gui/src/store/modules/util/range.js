
function isBetween (moment, start, end) {
  return moment.isBetween(start, end, null, '[)')
}
// start and end date are within range
function isEntryBetween (entry, start, end) {
  return entry.start_time.isSameOrAfter(start) && entry.end_time.isBefore(end)
}

// either start or end date are within range
function isEntryBetweenCalendar (entry, start, end) {
  return isBetween(entry.start_time, start, end) || isBetween(entry.end_time, start, end)
}

function state () {
  return {
    fetchedStart: null,
    fetchedEnd: null
  }
}

const getters = {
  // entries with start and end time within range
  getRange (state) {
    return (start, end) => {
      return Object.values(state.set).filter(entry => isEntryBetween(entry, start, end))
    }
  },
  // either start or end within range
  getByCalendarRange (state) {
    return (start, end) => {
      return Object.values(state.set).filter(entry => isEntryBetweenCalendar(entry, start, end))
    }
  }
}

const actions = (api) => {
  return {
    fetchRange (context, parameters) {
      let start
      let end
      let refetch
      [start, end, refetch = false] = parameters
      // return already fetched entries
      if (context.fetchedStart !== null && context.fetchedEnd !== null) {
        if (start >= context.state.fetchedStart && end <= context.state.fetchedEnd && refetch !== true) {
          return context.state.set
        }
      }
      // make api call
      return api.getByRange(start, end, true).then((objs) => {
        context.commit('set', objs)
        if (context.state.fetchedStart === null || !start.isBetween(context.state.fetchedStart, context.state.fetchedEnd, null, '[]')) {
          context.commit('setFetchedStart', start)
        }
        if (context.state.fetchedEnd === null || !end.isBetween(context.state.fetchedStart, context.state.fetchedEnd, null, '[]')) {
          context.commit('setFetchedEnd', end)
        }
        return objs
      })
    }
  }
}

const mutations = {
  setFetchedStart (state, start) {
    state.fetchedStart = start
  },
  setFetchedEnd (state, end) {
    state.fetchedEnd = end
  }
}

export {
  state,
  getters,
  actions,
  mutations
}
