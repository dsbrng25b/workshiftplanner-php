
const fixResources = [
  { id: 'openinghour', title: 'Öffnungszeiten' },
  { id: 'absence', title: 'Absenzen' }
]

const state = () => {
  return {}
}
const getters = {
  // convert absences to full calendar format
  getAbsencesByRange (state, getters, rootState, rootGetter) {
    return (start, end) => {
      return rootGetter['absence/getRange'](start, end).map((a) => {
        let fca = Object.assign({}, a)
        let person = rootGetter['person/get'](a.person_id)
        fca.start = a.start_time
        fca.end = a.end_time
        fca.color = person.color
        fca.title = person.short_name + ' ' + a.reason
        fca.realId = a.id
        fca.id = 'a-' + a.id
        fca.resourceId = 'absence'
        return fca
      })
    }
  },
  // convert workshifts to full calendar format
  getWorkshiftsByRange (state, getters, rootState, rootGetter) {
    return (start, end) => {
      return rootGetter['workshift/getRange'](start, end).map((w) => {
        let fcw = Object.assign({}, w)
        let person = rootGetter['person/get'](w.person_id)
        let work = rootGetter['work/get'](w.work_id)
        fcw.start = w.start_time
        fcw.end = w.end_time
        fcw.color = person.color
        fcw.title = person.short_name + ' ' + work.name
        fcw.realId = w.id
        fcw.id = 'w-' + w.id
        fcw.resourceId = work.id
        return fcw
      })
    }
  },
  // convert events to full calendar format
  getEventsByRange (state, getters, rootState, rootGetter) {
    return (start, end) => {
      return rootGetter['event/getRange'](start, end).map((e) => {
        let fce = Object.assign({}, e)
        fce.start = e.start_time
        fce.end = e.end_time
        fce.rendering = 'inverse-background'
        fce.color = '#c3c3c3'
        fce.resourceId = 'openinghour'
        fce.realId = e.id
        fce.id = 'openinghour'
        fce.editable = false
        return fce
      })
    }
  },
  getResources (state, getters, rootState, rootGetter) {
    let remoteResources = rootGetter['work/getAll'].map((resource) => {
      let r = Object.assign({}, resource)
      r.title = r.name
      return r
    })
    return fixResources.concat(remoteResources)
  },
  getRangeFilter (state, getters) {
    return (start, end, filter = {}) => {
      let entries = getters.getEventsByRange(start, end)
      if (filter.absences === true) {
        entries = entries.concat(getters.getAbsencesByRange(start, end))
      }
      let workId = parseInt(filter.work)
      if (isNaN(workId) || workId === 0) {
        entries = entries.concat(getters.getWorkshiftsByRange(start, end))
      } else {
        entries = entries.concat(getters.getWorkshiftsByRange(start, end).filter(w => w.work_id === workId))
      }
      return entries
    }
  },
  getRange (state, getters) {
    return (start, end, filter = null) => {
      // filtered
      if (filter) {
        return getters.getRangeFilter(start, end, filter)
      }
      // all
      return [].concat(
        getters.getAbsencesByRange(start, end),
        getters.getWorkshiftsByRange(start, end),
        getters.getEventsByRange(start, end)
      )
    }
  }
}

const actions = {
  fetchResources ({dispatch}) {
    return dispatch('work/fetchAll', null, {root: true})
  },
  fetchRange ({dispatch}, parameters) {
    let start
    let end
    let refetch
    [start, end, refetch = false] = parameters
    // get base data (person, work)
    return Promise.all([
      dispatch('person/fetchAll', null, {root: true}),
      dispatch('work/fetchAll', null, {root: true})
    ]).then(() => {
      // get calendar data
      return Promise.all([
        dispatch('absence/fetchRange', [start, end, refetch], {root: true}),
        dispatch('workshift/fetchRange', [start, end, refetch], {root: true}),
        dispatch('event/fetchRange', [start, end, refetch], {root: true})
      ])
    })
  }
}

const mutations = {}

export default{
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
