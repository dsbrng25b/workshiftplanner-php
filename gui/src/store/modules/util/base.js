import Vue from 'vue'

// const state = {
//   set: {}
// }

function state () {
  return {
    set: {}
  }
}

const getters = {
  getAll (state) {
    return Object.values(state.set)
  },
  get (state) {
    return id => state.set[id]
  }
}

const actions = (api) => {
  return {
    fetchAll (context, refetch = false) {
      if (refetch !== true && Object.values(context.state.set).length !== 0) {
        return context.state.set
      }
      return api.getAll().then((objs) => {
        context.commit('set', objs)
        return objs
      })
    },
    fetch (context, id, refetch = false) {
      if (refetch !== true && context.state.set.hasOwnProperty(id)) {
        return context.state.set[id]
      }
      return api.get(id).then((obj) => {
        context.commit('set', obj)
        return obj
      })
    },
    remove (context, obj) {
      return api.remove(obj).then(() => {
        context.commit('remove', obj)
      })
    },
    save (context, obj) {
      return api.save(obj).then((o) => {
        context.commit('set', o)
        return o
      })
    }
  }
}

const mutations = {
  set (state, objs) {
    if (objs.constructor !== Array) {
      objs = [objs]
    }
    for (let obj of objs) {
      Vue.set(state.set, obj.id, obj)
    }
  },
  remove (state, obj) {
    Vue.delete(state.set, obj.id)
  }
}

export {
  state,
  getters,
  actions,
  mutations
}
