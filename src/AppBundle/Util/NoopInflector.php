<?php

namespace AppBundle\Util;

use FOS\RestBundle\Inflector\InflectorInterface;

/**
 * NoopInflector class
 *
 */
class NoopInflector implements InflectorInterface
{
    public function pluralize($word)
    {
        // Don't pluralize
        return $word;
    }
}