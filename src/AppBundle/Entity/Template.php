<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Template
 *
 * @ORM\Table(name="template")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TemplateRepository")
 */
class Template
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="WorkshiftTemplate", mappedBy="template")
     */
    private $workshifts;

    /**
     * Template constructor.
     */
    public function __construct()
    {
        $this->workshifts = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Template
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set workshifts
     *
     * @param array $workshifts
     *
     * @return Template
     */
    public function setWorkshifts($workshifts)
    {
        $this->workshifts = $workshifts;

        return $this;
    }

    /**
     * Get workshifts
     *
     * @return array
     */
    public function getWorkshifts()
    {
        return $this->workshifts;
    }


    /**
     * Add workshift
     *
     * @param \AppBundle\Entity\WorkshiftTemplate $workshift
     *
     * @return Template
     */
    public function addWorkshift(\AppBundle\Entity\WorkshiftTemplate $workshift)
    {
        $this->workshifts[] = $workshift;

        return $this;
    }

    /**
     * Remove workshift
     *
     * @param \AppBundle\Entity\WorkshiftTemplate $workshift
     */
    public function removeWorkshift(\AppBundle\Entity\WorkshiftTemplate $workshift)
    {
        $this->workshifts->removeElement($workshift);
    }
}
