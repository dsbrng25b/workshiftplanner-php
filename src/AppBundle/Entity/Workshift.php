<?php

namespace AppBundle\Entity;

use AppBundle\Service\TimeZoneService;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Workshift
 *
 * @ORM\Table(name="workshift")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorkshiftRepository")
 */
class Workshift
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTime", type="datetime")
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endTime", type="datetime")
     */
    private $endTime;

    /**
     * @var Work
     *
     * @ORM\ManyToOne(targetEntity="Work")
     * @ORM\JoinColumn(name="work_id", referencedColumnName="id")
     * @Serializer\Exclude()
     *
     */
    private $work;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="workshifts")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Exclude()
     *
     */
    private $person;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Absence
     */
    public function setStartTime($startTime)
    {
        $this->startTime = TimeZoneService::convertToUTC($startTime);

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return TimeZoneService::ensureUTC($this->startTime);
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Absence
     */
    public function setEndTime($endTime)
    {
        $this->endTime = TimeZoneService::convertToUTC($endTime);

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return TimeZoneService::ensureUTC($this->endTime);
    }

    /**
     * Set work
     *
     * @param \stdClass $work
     *
     * @return Workshift
     */
    public function setWork($work)
    {
        $this->work = $work;

        return $this;
    }

    /**
     * Get work
     *
     * @return Work
     */
    public function getWork()
    {
        return $this->work;
    }
    /**
     * Get work_id
     *
     * @return int
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("work_id")
     */
    public function getWorkId()
    {
        return $this->work->getId();
    }

    /**
     * Set person
     *
     * @param \stdClass $person
     *
     * @return Workshift
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Get person_id
     *
     * @return int
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("person_id")
     */
    public function getPersonId()
    {
        return $this->person->getId();
    }
}
