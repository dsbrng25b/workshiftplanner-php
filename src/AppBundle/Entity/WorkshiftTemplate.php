<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WorkshiftTemplate
 *
 * @ORM\Table(name="workshift_template")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorkshiftTemplateRepository")
 */
class WorkshiftTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTime", type="time")
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endTime", type="time")
     */
    private $endTime;

    /**
     * @var int
     *
     * @ORM\Column(name="weekday", type="integer")
     */
    private $weekday;

    /**
     * @var Work
     *
     * @ORM\ManyToOne(targetEntity="Work")
     * @ORM\JoinColumn(name="work_id", referencedColumnName="id")
     */
    private $work;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    private $person;

    /**
     * @var Template
     *
     * @ORM\ManyToOne(targetEntity="Template", inversedBy="workshifts")
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    private $template;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return WorkshiftTemplate
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return WorkshiftTemplate
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set weekday
     *
     * @param integer $weekday
     *
     * @return WorkshiftTemplate
     */
    public function setWeekday($weekday)
    {
        $this->weekday = $weekday;

        return $this;
    }

    /**
     * Get weekday
     *
     * @return int
     */
    public function getWeekday()
    {
        return $this->weekday;
    }

    /**
     * Set work
     *
     * @param \stdClass $work
     *
     * @return WorkshiftTemplate
     */
    public function setWork($work)
    {
        $this->work = $work;

        return $this;
    }

    /**
     * Get work
     *
     * @return \stdClass
     */
    public function getWork()
    {
        return $this->work;
    }

    /**
     * Set person
     *
     * @param \stdClass $person
     *
     * @return WorkshiftTemplate
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \stdClass
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set template
     *
     * @param \stdClass $template
     *
     * @return WorkshiftTemplate
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return \stdClass
     */
    public function getTemplate()
    {
        return $this->template;
    }
}
