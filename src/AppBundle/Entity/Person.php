<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 */
class Person implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="shortName", type="string", length=8)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @var int
     *
     * @ORM\Column(name="employmentLevel", type="smallint")
     */
    private $employmentLevel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="contractStart", type="date", nullable=true)
     */
    private $contractStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="contractEnd", type="date", nullable=true)
     */
    private $contractEnd;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Absence", mappedBy="person")
     * @Serializer\Exclude()
     */
    private $absences;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Workshift", mappedBy="person")
     * @Serializer\Exclude()
     */
    private $workshifts;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique = true, nullable = true)
     */
    private $username;

    /**
     * @var bool
     *
     * @ORM\Column(name="isAdmin", type="boolean", options={"default" : false})
     */
    private $isAdmin = false;

    /**
     * Person constructor.
     */
    public function __construct()
    {
        $this->absences = new ArrayCollection();
        $this->workshifts = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Person
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return Person
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Person
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set employmentLevel
     *
     * @param integer $employmentLevel
     *
     * @return Person
     */
    public function setEmploymentLevel($employmentLevel)
    {
        $this->employmentLevel = $employmentLevel;

        return $this;
    }

    /**
     * Get employmentLevel
     *
     * @return int
     */
    public function getEmploymentLevel()
    {
        return $this->employmentLevel;
    }

    /**
     * Set contractStart
     *
     * @param \DateTime $contractStart
     *
     * @return Person
     */
    public function setContractStart($contractStart)
    {
        $this->contractStart = $contractStart;

        return $this;
    }

    /**
     * Get contractStart
     *
     * @return \DateTime
     */
    public function getContractStart()
    {
        return $this->contractStart;
    }

    /**
     * Set contractEnd
     *
     * @param \DateTime $contractEnd
     *
     * @return Person
     */
    public function setContractEnd($contractEnd)
    {
        $this->contractEnd = $contractEnd;

        return $this;
    }

    /**
     * Get contractEnd
     *
     * @return \DateTime
     */
    public function getContractEnd()
    {
        return $this->contractEnd;
    }

    /**
     * Get absences
     *
     * @return Collection
     */
    public function getAbsences()
    {
        return $this->absences;
    }

    /**
     * Get workshifts
     *
     * @return Collection
     */
    public function getWorkshifts()
    {
        return $this->workshifts;
    }

    /**
     * Add absence
     *
     * @param \AppBundle\Entity\Absence $absence
     *
     * @return Person
     */
    public function addAbsence(\AppBundle\Entity\Absence $absence)
    {
        $this->absences[] = $absence;

        return $this;
    }

    /**
     * Remove absence
     *
     * @param \AppBundle\Entity\Absence $absence
     */
    public function removeAbsence(\AppBundle\Entity\Absence $absence)
    {
        $this->absences->removeElement($absence);
    }

    /**
     * Add workshift
     *
     * @param \AppBundle\Entity\Workshift $workshift
     *
     * @return Person
     */
    public function addWorkshift(\AppBundle\Entity\Workshift $workshift)
    {
        $this->workshifts[] = $workshift;

        return $this;
    }

    /**
     * Remove workshift
     *
     * @param \AppBundle\Entity\Workshift $workshift
     */
    public function removeWorkshift(\AppBundle\Entity\Workshift $workshift)
    {
        $this->workshifts->removeElement($workshift);
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Person
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     *
     * @return Person
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    public function getRoles()
    {
        if($this->isAdmin){
            return ['ROLE_ADMIN'];
        }

        return ['ROLE_USER'];
    }

    public function getPassword()
    {
        return null;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {

    }


}
