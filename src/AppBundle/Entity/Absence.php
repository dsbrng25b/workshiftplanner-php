<?php

namespace AppBundle\Entity;

use AppBundle\Service\TimeZoneService;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Absence
 *
 * @ORM\Table(name="absence")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AbsenceRepository")
 */
class Absence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTime", type="datetime")
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endTime", type="datetime")
     */
    private $endTime;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255)
     */
    private $reason;

    /**
     * @var bool
     *
     * @ORM\Column(name="optional", type="boolean")
     */
    private $optional;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="absences")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", onDelete="CASCADE")
     * @Serializer\Exclude()
     */
    private $person;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Absence
     */
    public function setStartTime($startTime)
    {
        $this->startTime = TimeZoneService::convertToUTC($startTime);

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return TimeZoneService::ensureUTC($this->startTime);
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Absence
     */
    public function setEndTime($endTime)
    {
        $this->endTime = TimeZoneService::convertToUTC($endTime);

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return TimeZoneService::ensureUTC($this->endTime);
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return Absence
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set optional
     *
     * @param boolean $optional
     *
     * @return Absence
     */
    public function setOptional($optional)
    {
        $this->optional = $optional;

        return $this;
    }

    /**
     * Get optional
     *
     * @return bool
     */
    public function getOptional()
    {
        return $this->optional;
    }

    /**
     * Set person
     *
     * @param \stdClass $person
     *
     * @return Absence
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \stdClass
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Get person_id
     *
     * @return int
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("person_id")
     */
    public function getPersonId()
    {
        return $this->person->getId();
    }

}
