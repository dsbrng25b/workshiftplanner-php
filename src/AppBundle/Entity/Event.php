<?php

namespace AppBundle\Entity;

use AppBundle\Service\TimeZoneService;
use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    const TYPE_OPENING_HOUR = 0;
    const TYPE_REGULAR_EVENT = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTime", type="datetime")
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endTime", type="datetime")
     */
    private $endTime;

    /**
     * @var int
     *
     * @ORM\Column(name="typ", type="integer")
     */
    private $typ;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Absence
     */
    public function setStartTime($startTime)
    {
        $this->startTime = TimeZoneService::convertToUTC($startTime);

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return TimeZoneService::ensureUTC($this->startTime);
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Absence
     */
    public function setEndTime($endTime)
    {
        $this->endTime = TimeZoneService::convertToUTC($endTime);

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return TimeZoneService::ensureUTC($this->endTime);
    }

    /**
     * Set typ
     *
     * @param integer $typ
     *
     * @return Event
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;

        return $this;
    }

    /**
     * Get typ
     *
     * @return int
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
