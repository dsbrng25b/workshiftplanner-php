<?php

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class OpeningHourApi
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     *
     * @return array
     */
    public function fetch(\DateTime $startDate, \DateTime $endDate){
        $currentDate = $startDate;

        $result = [];
        while($currentDate <= $endDate) {
            $beforeCall = $currentDate->format('Y-m-d');
            $result = array_merge($result, $this->fetchTwoWeeks($currentDate));

            if($currentDate->format('Y-m-d') == $beforeCall){
                break;
            }

            $currentDate->add(new \DateInterval('P1D'));
        }

        return $this->limitToEndDate($result, $endDate);
    }

    /**
     * @param \DateTime $startDate
     * @return array
     *
     * @throws \Requests_Exception_HTTP
     */
    private function fetchTwoWeeks($startDate){
        $ubPubUri = $this->container->getParameter('ub_pub_uri');
        $ubPubLocationId = $this->container->getParameter('ub_pub_location_id');

        // Generating the endpoint uri by using week number and day number (1 - 7) of given start date
        $endpoint = $ubPubUri . "library_locations/" . $ubPubLocationId . "/library_schedules.json?y=" . $startDate->format('Y') . "&w=" . $startDate->format('W') . "&d=" .$startDate->format('N');

        $headers = ['Accept' => 'application/json'];

        $request = \Requests::get($endpoint, $headers);

        // Will throw an exception if final status (after redirects) is not 200
        $request->throw_for_status();


        $currentDate = $startDate;

        $data = (json_decode($request->body))->events;
        $lastEvent = -1;
        $openingHours = [];

        foreach ($data as $day) {
            //Ensure that sunday is day number 0
            if ($day->day_number == 7) {
                $day->day_number = 0;
            }

            // When the last event had 0 as index and the current also, it is at least the next day
            // This prevents problems (not increasing date), if a location is closed exactly a week.
            if ($lastEvent == 0 && $day->event_number == 0) {
                $currentDate->add(new \DateInterval('P1D'));
            }

            //Adding a day to current date until we met the weekday number
            while ($currentDate->format('w') != $day->day_number) {
                $currentDate->add(new \DateInterval('P1D'));
            }

            $openingHours[] = [
                'start' => new \DateTime($currentDate->format('Y-m-d ') . intdiv($day->minutes_start, 60) . ':' . ($day->minutes_start % 60), new \DateTimeZone('Europe/Zurich')),
                'end' => new \DateTime($currentDate->format('Y-m-d ') . intdiv($day->minutes_end, 60) . ':' . ($day->minutes_end % 60), new \DateTimeZone('Europe/Zurich')),
            ];

            $lastEvent = $day->event_number;
        }

        return $openingHours;
    }

    /**
     * @param array $openingHours
     * @return array with objects
     */
    private function createOpeningHourObjects(array $openingHours){
        $result = [];

        foreach($openingHours as $date => $openingHoursItems){
            $date = new \DateTime($date, new \DateTimeZone('Europe/Zurich'));
            $object = new \stdClass();
            $object->date = $date;
            foreach($openingHoursItems as $item) {
                $object->openingHours[] = (object) $item;
            }

            $result[] = $object;
        }

        return $result;
    }

    /**
     * @param $openingHours
     * @param $endDate
     * @return array
     */
    private function limitToEndDate(&$openingHours, \DateTime $endDate){
        $endDate = new \DateTime($endDate->format('Y-m-d'));
        foreach($openingHours as $key => $item){
            if(new \DateTime($item['start']->format('Y-m-d')) > $endDate){
                unset($openingHours[$key]);
            }
        }

        return array_values($openingHours);
    }
}