<?php

namespace AppBundle\Service;


class TimeZoneService
{
    /**
     * Sets the timezone to UTC, date and time value will be converted.
     *
     * @param \DateTime $date
     * @return \DateTime
     */
    public static function convertToUTC(\DateTime $date = null){
        if ($date == null) {
            return null;
        }
        $date->setTimezone(new \DateTimeZone("UTC"));
        return $date;
    }

    /**
     * Uses the date time string without timezone information and returns
     * a new DateTime object with timezone set to UTC.
     *
     * @param \DateTime $date
     * @return \DateTime
     */
    public static function ensureUTC(\DateTime $date = null){
        if ($date == null){
            return null;
        }
        $dateString = $date->format('Y-m-d H:i:s');
        return new \DateTime($dateString, new \DateTimeZone("UTC"));
    }
}