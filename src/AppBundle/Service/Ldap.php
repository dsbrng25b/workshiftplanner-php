<?php

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class Ldap
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var string
     */
    private $loginError;

    /**
     * Ldap constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $username the ldap username. Existing domain information will be removed.
     * @param string $password the ldap password for the given user.
     * @return bool true if login was successful, false otherwise.
     */
    public function ldapLogin($username, $password)
    {
        $username = $this->removeDomainInformation($username);

        if(empty($username) || empty($password) || !$this->connectToLdapServer($username, $password)) {
            $this->loginError = "INVALID_CREDENTIALS";
            return false;
        }

        return true;
    }

    /**
     * @return null|string symfony login error string if ldap login was not successful, null otherwise.
     */
    public function getLoginError(){
        return $this->loginError;
    }

    private function connectToLdapServer($username, $password)
    {
        $ldapServer = "ldap://" . $this->container->getParameter('ldap_server');
        $ldapDomain = $this->container->getParameter('ldap_domain');

        $ldapConnection = ldap_connect($ldapServer);
        ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapConnection, LDAP_OPT_REFERRALS, 0);

        return @ldap_bind($ldapConnection, $ldapDomain . "\\" . $username, $password);
    }

    private function removeDomainInformation($username)
    {
        if(!empty($username))
        {
            if(strstr('@', $username)){
                $username = explode("@", $user)[0];
            }

            if(strstr('\\', $username)){
                $username = array_pop(explode('\\', $username));
            }
        }

        return $username;
    }
}