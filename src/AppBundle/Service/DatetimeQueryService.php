<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

class DatetimeQueryService
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Returns all entities (workshifts, absences, events) within a certain time range
     *
     * @param $className
     * @param string|null $startTime
     * @param string|null $endTime
     * @return array
     */
    public function getInsideRange($className, $startTime = null, $endTime = null)
    {

        $query = $this->em->getRepository($className)->createQueryBuilder('t');

        if ($startTime != null) {
            $startTime = \DateTime::createFromFormat(\DateTime::ISO8601, $startTime);
            $query = $query->andWhere('t.startTime > :startTime')->setParameter('startTime', $startTime);
        }

        if ($endTime != null) {
            $endTime = \DateTime::createFromFormat(\DateTime::ISO8601, $endTime);
            $query = $query->andWhere('t.endTime < :endTime')->setParameter('endTime', $endTime);
        }

        $query = $query->getQuery();

        return $query->getResult();
    }

    /**
     * Return all entities which have either the startTime or the endTime within a certain time range
     *
     * @param $className
     * @param string $startTime
     * @param string $endTime
     * @return array
     */
    public function getCalendarRange($className, $startTime, $endTime)
    {

        $startTime = \DateTime::createFromFormat(\DateTime::ISO8601, $startTime);
        $endTime = \DateTime::createFromFormat(\DateTime::ISO8601, $endTime);

        $query = $this->em->getRepository($className)->createQueryBuilder('t')
            ->where('t.startTime BETWEEN :startTime AND :endTime')
            ->orWhere('t.endTime BETWEEN :startTime AND :endTime')
            ->setParameters([
                'startTime' => $startTime,
                'endTime' => $endTime
            ])->getQuery();

        return $query->getResult();

    }

}
