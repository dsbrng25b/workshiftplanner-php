<?php

namespace AppBundle\Controller;

use AppBundle\Form\PersonType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use AppBundle\Entity\Person;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("person")
 */
class PersonApiController extends FOSRestController
{

    public function getAction(Person $person)
    {
        $view = $this->view($person, 200);
        return $this->handleView($view);
    }

    public function cgetAction()
    {
        $em = $this->getDoctrine()->getManager();
        $persons = $em->getRepository(Person::class)->findAll();
        $view = $this->view($persons, 200);
        return $this->handleView($view);
    }

    public function cpostAction(Request $request)
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();
            return $this->handleView($this->view( $person, 201));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function putAction(Person $person, Request $request)
    {
        $form = $this->createForm(PersonType::class, $person, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();
            return $this->handleView($this->view( $person, 204));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function deleteAction(Person $person, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($person);
        $em->flush();
        return $this->handleView($this->view(null, 204));
    }
}