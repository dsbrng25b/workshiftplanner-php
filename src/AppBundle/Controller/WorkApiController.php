<?php

namespace AppBundle\Controller;

use AppBundle\Form\WorkType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use AppBundle\Entity\Work;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("work")
 */
class WorkApiController extends FOSRestController
{

    public function getAction(Work $work)
    {
        $view = $this->view($work, 200);
        return $this->handleView($view);
    }

    public function cgetAction()
    {
        $em = $this->getDoctrine()->getManager();
        $works = $em->getRepository(Work::class)->findAll();
        $view = $this->view($works, 200);
        return $this->handleView($view);
    }

    public function cpostAction(Request $request)
    {
        $work = new Work();
        $form = $this->createForm(WorkType::class, $work, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($work);
            $em->flush();
            return $this->handleView($this->view( $work, 201));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function putAction(Work $work, Request $request)
    {
        $form = $this->createForm(WorkType::class, $work, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($work);
            $em->flush();
            return $this->handleView($this->view( $work, 204));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function deleteAction(Work $work, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($work);
        $em->flush();
        return $this->handleView($this->view(null, 204));
    }
}