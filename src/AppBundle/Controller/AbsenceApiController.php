<?php

namespace AppBundle\Controller;

use AppBundle\Form\AbsenceType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use AppBundle\Entity\Absence;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("absence")
 */
class AbsenceApiController extends FOSRestController
{

    public function getAction(Absence $absence)
    {
        $view = $this->view($absence, 200);
        return $this->handleView($view);
    }

    public function cgetAction(Request $request)
    {
        $startTime = $request->query->get('start');
        $endTime = $request->query->get('end');
        $calendar = $request->query->get('calendar');

        $dqs = $this->container->get('datetime_query');

        $absences = [];
        if ($calendar != null) {
            $absences = $dqs->getCalendarRange(Absence::class, $startTime, $endTime);
        } else {
            $absences = $dqs->getInsideRange(Absence::class, $startTime, $endTime);
        }
        $view = $this->view($absences, 200);
        return $this->handleView($view);
    }

    public function cpostAction(Request $request)
    {
        $absence = new Absence();
        $form = $this->createForm(AbsenceType::class, $absence, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($absence);
            $em->flush();
            return $this->handleView($this->view( $absence, 201));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function putAction(Absence $absence, Request $request)
    {
        $form = $this->createForm(AbsenceType::class, $absence, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($absence);
            $em->flush();
            return $this->handleView($this->view( $absence, 204));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function deleteAction(Absence $absence, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($absence);
        $em->flush();
        return $this->handleView($this->view(null, 204));
    }
}