<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Security controller.
 */
class SecurityController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/login", name="login")
     * @Method({"GET", "POST"})
     */
    public function loginAction(Request $request)
    {
        $username = null;
        $error = null;

        if($request->getMethod() == 'POST')
        {

            $username = $request->get('username');
            $password = $request->get('password');


            if($username != $this->container->getParameter('local_admin_username') && $password != $this->container->getParameter('local_admin_password')) {
                /* @var \AppBundle\Service\Ldap $ldap */
                $ldap = $this->container->get('ldap');

                if (!$ldap->ldapLogin($username, $password)) {
                    $error = $ldap->getLoginError();
                }
            }

            if(empty($error)){
                try {
                    $userProvider = $this->container->get('user_provider');
                    $user = $userProvider->loadUserByUsername($username);
                    $this->loginUser($user);

                    return $this->redirectToRoute('homepage');
                }catch(UsernameNotFoundException $exception){
                    $error = "NOT_ALLOWED";
                }
            }
        }

        return $this->render('login/login.html.twig', ['username' => $username, 'error' => $error]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/logout", name="logout")
     * @Method({"GET"})
     */
    public function logoutAction(Request $request)
    {
        /* @var \Symfony\Component\Security\Core\User\UserInterface $user */
        $user = $this->container->get("security.token_storage")->getToken()->getUser();
        $this->logoutUser();
        return $this->redirectToRoute('login');
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/api/user", name="api_user")
     * @Method({"GET"})
     */
    public function userAction(Request $request)
    {
        /* @var \Symfony\Component\Security\Core\User\UserInterface $user */
        $user = $this->container->get("security.token_storage")->getToken()->getUser();

        $isAdmin = false;
        if(in_array('ROLE_ADMIN', $user->getRoles())){
            $isAdmin = true;
        }

        return $this->json(['username' => $user->getUsername(), 'isAdmin' => $isAdmin]);
    }



    private function loginUser(UserInterface $user){
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles()) ;

        $this->container->get("security.token_storage")->setToken($token);
        $this->container->get('session')->set('_security_main', serialize($token));
    }

    private function logoutUser(){
        $this->get('security.token_storage')->setToken(null);
    }
}
