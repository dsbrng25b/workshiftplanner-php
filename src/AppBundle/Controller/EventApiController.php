<?php

namespace AppBundle\Controller;

use AppBundle\Form\EventType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use AppBundle\Entity\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("event")
 */
class EventApiController extends FOSRestController
{

    public function getAction(Event $event)
    {
        $view = $this->view($event, 200);
        return $this->handleView($view);
    }

    public function cgetAction(Request $request)
    {
        $startTime = $request->query->get('start');
        $endTime = $request->query->get('end');
        $calendar = $request->query->get('calendar');

        $dqs = $this->container->get('datetime_query');

        $events = [];
        if ($calendar != null) {
            $events = $dqs->getCalendarRange(Event::class, $startTime, $endTime);
        } else {
            $events = $dqs->getInsideRange(Event::class, $startTime, $endTime);
        }
        $view = $this->view($events, 200);
        return $this->handleView($view);
    }

    public function cpostAction(Request $request)
    {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();
            return $this->handleView($this->view( $event, 201));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function putAction(Event $event, Request $request)
    {
        $form = $this->createForm(EventType::class, $event, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();
            return $this->handleView($this->view( $event, 204));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function deleteAction(Event $event, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($event);
        $em->flush();
        return $this->handleView($this->view(null, 204));
    }
}