<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Workshift;
use AppBundle\Form\WorkshiftType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("workshift")
 */
class WorkshiftApiController extends FOSRestController
{

    public function getAction(Workshift $workshift)
    {
        $view = $this->view($workshift, 200);
        return $this->handleView($view);
    }

    public function cgetAction(Request $request)
    {
        $startTime = $request->query->get('start');
        $endTime = $request->query->get('end');
        $calendar = $request->query->get('calendar');

        $dqs = $this->container->get('datetime_query');

        $workshifts = [];
        if ($calendar != null) {
            $workshifts = $dqs->getCalendarRange(Workshift::class, $startTime, $endTime);
        } else {
            $workshifts = $dqs->getInsideRange(Workshift::class, $startTime, $endTime);
        }
        $view = $this->view($workshifts, 200);
        return $this->handleView($view);
    }

    public function cpostAction(Request $request)
    {
        $workshift = new Workshift();
        $form = $this->createForm(WorkshiftType::class, $workshift, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($workshift);
            $em->flush();
            return $this->handleView($this->view( $workshift, 201));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function putAction(Workshift $workshift, Request $request)
    {
        $form = $this->createForm(WorkshiftType::class, $workshift, array("csrf_protection" => false));
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($workshift);
            $em->flush();
            return $this->handleView($this->view( $workshift, 204));
        }
        return $this->handleView($this->view($form, 400));
    }

    public function deleteAction(Workshift $workshift, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($workshift);
        $em->flush();
        return $this->handleView($this->view(null, 204));
    }
}