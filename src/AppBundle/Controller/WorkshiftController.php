<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Workshift;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Workshift controller.
 *
 * @Route("workshift")
 */
class WorkshiftController extends Controller
{
    /**
     * Lists all workshift entities.
     *
     * @Route("/", name="workshift_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $workshifts = $em->getRepository('AppBundle:Workshift')->findAll();

        return $this->render('workshift/index.html.twig', array(
            'workshifts' => $workshifts,
        ));
    }

    /**
     * Creates a new workshift entity.
     *
     * @Route("/new", name="workshift_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $workshift = new Workshift();
        $form = $this->createForm('AppBundle\Form\WorkshiftType', $workshift);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($workshift);
            $em->flush();

            return $this->redirectToRoute('workshift_show', array('id' => $workshift->getId()));
        }

        return $this->render('workshift/new.html.twig', array(
            'workshift' => $workshift,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a workshift entity.
     *
     * @Route("/{id}", name="workshift_show")
     * @Method("GET")
     */
    public function showAction(Workshift $workshift)
    {
        $deleteForm = $this->createDeleteForm($workshift);

        return $this->render('workshift/show.html.twig', array(
            'workshift' => $workshift,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing workshift entity.
     *
     * @Route("/{id}/edit", name="workshift_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Workshift $workshift)
    {
        $deleteForm = $this->createDeleteForm($workshift);
        $editForm = $this->createForm('AppBundle\Form\WorkshiftType', $workshift);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('workshift_edit', array('id' => $workshift->getId()));
        }

        return $this->render('workshift/edit.html.twig', array(
            'workshift' => $workshift,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a workshift entity.
     *
     * @Route("/{id}", name="workshift_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Workshift $workshift)
    {
        $form = $this->createDeleteForm($workshift);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($workshift);
            $em->flush();
        }

        return $this->redirectToRoute('workshift_index');
    }

    /**
     * Creates a form to delete a workshift entity.
     *
     * @param Workshift $workshift The workshift entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Workshift $workshift)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('workshift_delete', array('id' => $workshift->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
