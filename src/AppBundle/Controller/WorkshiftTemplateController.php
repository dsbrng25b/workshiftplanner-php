<?php

namespace AppBundle\Controller;

use AppBundle\Entity\WorkshiftTemplate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Workshifttemplate controller.
 *
 * @Route("workshifttemplate")
 */
class WorkshiftTemplateController extends Controller
{
    /**
     * Lists all workshiftTemplate entities.
     *
     * @Route("/", name="workshifttemplate_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $workshiftTemplates = $em->getRepository('AppBundle:WorkshiftTemplate')->findAll();

        return $this->render('workshifttemplate/index.html.twig', array(
            'workshiftTemplates' => $workshiftTemplates,
        ));
    }

    /**
     * Creates a new workshiftTemplate entity.
     *
     * @Route("/new", name="workshifttemplate_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $workshiftTemplate = new Workshifttemplate();
        $form = $this->createForm('AppBundle\Form\WorkshiftTemplateType', $workshiftTemplate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($workshiftTemplate);
            $em->flush();

            return $this->redirectToRoute('workshifttemplate_show', array('id' => $workshiftTemplate->getId()));
        }

        return $this->render('workshifttemplate/new.html.twig', array(
            'workshiftTemplate' => $workshiftTemplate,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a workshiftTemplate entity.
     *
     * @Route("/{id}", name="workshifttemplate_show")
     * @Method("GET")
     */
    public function showAction(WorkshiftTemplate $workshiftTemplate)
    {
        $deleteForm = $this->createDeleteForm($workshiftTemplate);

        return $this->render('workshifttemplate/show.html.twig', array(
            'workshiftTemplate' => $workshiftTemplate,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing workshiftTemplate entity.
     *
     * @Route("/{id}/edit", name="workshifttemplate_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, WorkshiftTemplate $workshiftTemplate)
    {
        $deleteForm = $this->createDeleteForm($workshiftTemplate);
        $editForm = $this->createForm('AppBundle\Form\WorkshiftTemplateType', $workshiftTemplate);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('workshifttemplate_edit', array('id' => $workshiftTemplate->getId()));
        }

        return $this->render('workshifttemplate/edit.html.twig', array(
            'workshiftTemplate' => $workshiftTemplate,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a workshiftTemplate entity.
     *
     * @Route("/{id}", name="workshifttemplate_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, WorkshiftTemplate $workshiftTemplate)
    {
        $form = $this->createDeleteForm($workshiftTemplate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($workshiftTemplate);
            $em->flush();
        }

        return $this->redirectToRoute('workshifttemplate_index');
    }

    /**
     * Creates a form to delete a workshiftTemplate entity.
     *
     * @param WorkshiftTemplate $workshiftTemplate The workshiftTemplate entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(WorkshiftTemplate $workshiftTemplate)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('workshifttemplate_delete', array('id' => $workshiftTemplate->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
