<?php

namespace AppBundle\Command;


use AppBundle\Entity\Event;
use AppBundle\Entity\OpeningHour;
use AppBundle\Service\OpeningHourApi;
use AppBundle\Service\TimeZoneService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class OpeningHourUpdate extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:openinghours:update')
            ->setDescription('Fetches and updates the opening hours.')
            ->setHelp('This command allows you to fetch and update the opening hours')
            ->addOption(
                'date',
                null,
                InputOption::VALUE_REQUIRED,
                'If set, the task will use the given date as begin of the date range, otherwise the current date will be used.'
            )
            ->addOption(
                'days',
                null,
                InputOption::VALUE_REQUIRED,
                'If set, the task will use the given days to calculate the date range, otherwise the default days set in parameters will be used.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dateString = date("Y-m-d");

        if(!is_null($input->getOption('date'))){
            $dateString = $input->getOption('date');
        }

        $days = $this->getContainer()->getParameter('ub_pub_num_days_to_fetch');

        if(!is_null($input->getOption('days'))){
            $days = $input->getOption('days');
        }

        $startDate = new \DateTime($dateString, new \DateTimeZone('Europe/Zurich'));
        $endDate = new \DateTime($dateString, new \DateTimeZone('Europe/Zurich'));
        $endDate->add(new \DateInterval('P' . $days . 'D'));

        $output->writeln("");
        $output->writeln("Update opening hours");
        $output->writeln("====================");
        $output->writeln("Start date: " . $startDate->format('Y-m-d'));
        $output->writeln("Days: " . $days);
        $output->writeln("End date: " . $endDate->format('Y-m-d'));

        /* @var OpeningHourApi $openingHourApi */
        $openingHourApi = $this->getContainer()->get('opening_hour');

        $result = $openingHourApi->fetch($startDate, $endDate);

        /* @var EntityManagerInterface $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        /* @var \AppBundle\Repository\EventRepository $repo */
        $repo = $entityManager->getRepository('AppBundle:Event');

        $eventIds = [];

        foreach($result as $openingHour){
            /* @var \DateTime $startTime */
            $startTime = $openingHour['start'];
            /* @var \DateTime $endTime */
            $endTime = $openingHour['end'];

            /* @var \AppBundle\Entity\Event $event */
            $event = null;

            $events = $repo->findBy(['startTime' => TimeZoneService::convertToUTC($startTime), 'endTime' => TimeZoneService::convertToUTC($endTime), 'typ' => Event::TYPE_OPENING_HOUR]);

            if(count($events) > 0){
                $event = $events[0];
                for($i = 1; $i < count($events); $i++){
                    $entityManager->remove($events[$i]);
                }
            }

            if(is_null($event)){
                $output->writeln("Create new opening hour event (start: " . $startTime->format('Y-m-d H:i:s') . ", end: " . $endTime->format('Y-m-d H:i:s') . ")");
                $event = new Event();
                $event->setTyp(Event::TYPE_OPENING_HOUR);
                $event->setStartTime($startTime);
                $event->setEndTime($endTime);

                $entityManager->persist($event);
                $entityManager->flush();
                $entityManager->refresh($event);
            }else{
                $output->writeln("Opening hour already exists (start: " . $startTime->format('Y-m-d H:i:s') . ", end: " . $endTime->format('Y-m-d H:i:s') . ")");
            }

            $eventIds[] = $event->getId();
        }


        $startDate = new \DateTime($dateString, new \DateTimeZone('Europe/Zurich'));
        $endDate = new \DateTime($dateString, new \DateTimeZone('Europe/Zurich'));
        $endDate->add(new \DateInterval('P' . $days . 'D'));

        $removeEvents = $repo->findNotInList(Event::TYPE_OPENING_HOUR, $startDate, $endDate, $eventIds);

        foreach($removeEvents as $event){
            $output->writeln("Remove opening hour event (start: " . $event->getStartTime()->format('Y-m-d H:i:s') . ", end: " . $event->getEndTime()->format('Y-m-d H:i:s') . ")");
            $entityManager->remove($event);
        }

        $entityManager->flush();

        $output->writeln("");
        $output->writeln("Job finished...");
        $output->writeln("===============");
    }
}