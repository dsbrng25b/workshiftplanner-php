<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class LocalAdmin implements UserInterface, EquatableInterface
{
    public function getRoles()
    {
        return ['ROLE_ADMIN'];
    }

    public function getPassword()
    {
        return null;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return 'admin';
    }

    public function eraseCredentials()
    {

    }

    public function isEqualTo(UserInterface $user)
    {
        if(!$user instanceof LocalAdmin){
            return false;
        }

        if($this->getUsername() != $user->getUsername()){
            return false;
        }

        return true;
    }


}