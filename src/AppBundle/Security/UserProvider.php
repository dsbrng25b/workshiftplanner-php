<?php

namespace AppBundle\Security;

use AppBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserProvider implements UserProviderInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager){
        $this->entityManager = $entityManager;
        $this->container = $container;
    }


    public function loadUserByUsername($username)
    {
        if($username == $this->container->getParameter('local_admin_username')){
            return new LocalAdmin();
        }

        $person = $this->entityManager->getRepository('AppBundle:Person')->findOneBy(['username' => $username]);

        if (!is_null($person)) {
            return $person;
        }

        throw new UsernameNotFoundException(
            sprintf('Benutzer "%s" existiert nicht.', $username)
        );
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof LocalAdmin && !$user instanceof Person) {
            throw new UnsupportedUserException(
                sprintf('Instanzen von "%s" werden nicht unterstützt.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return LocalAdmin::class === $class || Person::class === $class;
    }
}