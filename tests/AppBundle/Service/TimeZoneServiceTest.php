<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\TimeZoneService;
use PHPUnit\Framework\TestCase;

class TimeZoneServiceTest extends TestCase
{
    public function testConvertToUtc(){
        $date = new \DateTime("2017-11-13 13:00:00", new \DateTimeZone("Europe/Zurich"));

        $date = TimeZoneService::convertToUTC($date);
        $this->assertEquals('2017-11-13T12:00:00+0000', $date->format(\DateTime::ISO8601));
    }

    public function testConvertToUtcDefaultTimeZone(){
        date_default_timezone_set('Europe/Zurich');

        $date = new \DateTime("2017-11-13 13:00:00");

        $date = TimeZoneService::convertToUTC($date);
        $this->assertEquals('2017-11-13T12:00:00+0000', $date->format(\DateTime::ISO8601));
    }

    public function testEnsureUtc(){
        $date = new \DateTime("2017-11-13 13:00:00", new \DateTimeZone("Europe/Zurich"));

        $date = TimeZoneService::ensureUTC($date);
        $this->assertEquals('2017-11-13T13:00:00+0000', $date->format(\DateTime::ISO8601));
    }

    public function testEnsureUtcDefaultTimeZone(){
        date_default_timezone_set('Europe/Zurich');

        $date = new \DateTime("2017-11-13 13:00:00");

        $date = TimeZoneService::ensureUTC($date);
        $this->assertEquals('2017-11-13T13:00:00+0000', $date->format(\DateTime::ISO8601));
    }
}